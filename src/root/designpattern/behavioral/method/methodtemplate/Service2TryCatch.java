package root.designpattern.behavioral.method.methodtemplate;

public class Service2TryCatch extends RepeatedTryCatch{

    @Override
    public Object execute(Object object) {
        square((Integer) object);
        throw new IndexOutOfBoundsException();
    }

    public void square(Integer integer){
        System.out.println("Service2TryCatch - square of " + integer + " is " +Math.pow(integer, 2));
    }
}
