package root.designpattern.behavioral.method.methodtemplate;

import java.util.Map;

public class RepeatedTryCatchRunner {

    public static void main(String[] args) {
        Service1TryCatch service1 = new Service1TryCatch();
        Object string = new String("Service1TryCatch");
        service1.executeInternal(string);

        Service2TryCatch service2 = new Service2TryCatch();
        Object integer = 10;
        service2.executeInternal(integer);

        Service3TryCatch service3 = new Service3TryCatch();
        Object obj = new Object();
        service3.executeInternal(obj);

        Service4TryCatch service4 = new Service4TryCatch();
        Object obj2 = "ksc";
        Map<String, Integer> map = (Map<String, Integer>)service4.executeInternal(obj2);
        System.out.println("RepeatedTryCatchRunner -> main :: " + map);
    }
}
