package root.designpattern.behavioral.method.methodtemplate;

public class Service3TryCatch extends RepeatedTryCatch{

    @Override
    public Object execute(Object obj) {
        printHashcode(obj);
        throw new RuntimeException();
    }

    private void printHashcode(Object obj) {
        System.out.println(obj);
    }
}
