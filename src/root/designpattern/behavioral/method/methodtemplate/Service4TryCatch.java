package root.designpattern.behavioral.method.methodtemplate;

import java.util.HashMap;
import java.util.Map;

public class Service4TryCatch extends RepeatedTryCatch {

    @Override
    public Object execute(Object object) {
        return map((String)object);
    }

    private Map<String, Integer> map(String string) {
        Map<String, Integer> map = new HashMap<>();
        map.put(string, string.length());
        return map;
    }
}
