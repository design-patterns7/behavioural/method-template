package root.designpattern.behavioral.method.methodtemplate;

public class Service1TryCatch extends RepeatedTryCatch {

    @Override
    public Object execute(Object object){
        toUpperCase((String)object);
        throw new NullPointerException();
    }

    private void toUpperCase(String string){
        System.out.println(string.toUpperCase());
    }
}
