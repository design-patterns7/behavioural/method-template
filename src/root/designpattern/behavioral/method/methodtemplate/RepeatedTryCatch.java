package root.designpattern.behavioral.method.methodtemplate;

public abstract class RepeatedTryCatch {

    protected final Object executeInternal(Object object){
        try{
            return execute(object);
        }catch (NullPointerException e){
            System.out.println("Null pointer exception occurred");
        }catch (IndexOutOfBoundsException e){
            System.out.println("Index out of bounds exception occurred");
        }catch (Exception e){
            System.out.println("generic exception occurred");
        }
        return null;
    }

    abstract public Object execute(Object ex);
}
